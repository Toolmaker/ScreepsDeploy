# Screeps Deploy

Basic grunt-screeps deploy script with file flattening of file hierarchy before uploading to the server.

Deploy file allows users to use a nested file hierarchy which gets flatten from `/dir/subdir/file.js` to `dir.subdir.file.js`

## Installation

`Copy package.json` and `Gruntfile.js` into your project root. Your sources is expected to be in `src/` and will be flattened into `dest/`.

An optional `.gitignore` is provided exclude all npm packages and the output directory.

Using `npm install` all the required packages can be installed (Grunt, grunt screeps, grunt-contrib-clean and grunt-contrib-copy). 

## Usage

Create a `secrets.json` file containing the following contents:
`
{
    "username": "your@email.address",
    "password": "yourpassword",
    "branch": "branch-name"
}`

Run `grunt deploy` to execute the script. 